use std::io::{self, Write};
use urlencoding::encode;

fn main() -> io::Result<()> {
    let mut phone = String::new();
    let mut message = String::new();

    print!("Digite um número: ");
    io::stdout().flush()?;
    io::stdin().read_line(&mut phone).unwrap();

    print!("Digite um texto: ");
    io::stdout().flush()?;
    io::stdin().read_line(&mut message).unwrap();

    let encoded_message = encode(message.trim()).to_string();
    let url = generate_whatspp_link(phone.trim(), encoded_message);
    println!("Chat WhatsApp {}", url);

    Ok(())
}

fn generate_whatspp_link(phone: &str, message: String) -> String {
    let url = format!("https://wa.me/55{phone}?text={message}");
    return url;
}
