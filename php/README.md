# WhatsApp Link Generator

## Console
```bash
$ bin/whatsapplink -p 5569984990001 -t "Olá como vai?"
```
### Opções
* p : phone : Telefone no formato válido 5569988880000
* t : text : (opcional) Mensagem a ser enviada

## API Servidor HTTP

### Inicie o servidor
1. Inicie o servido via composer. Por padrão a porta é 8000

```bash
$ composer serve
```

Ou manualmente via PHP

```bash
$ php -S 0.0.0.0:8000 -t public/ public/index.php
```

2. Efetue a requisição no navegador
```
http://localhost:8000/?phone=5569984990001&text=Ol%C3%A1&format=html
```

### Parâmetros
* phone: Telefone
* text: (opcional) Mensagem de texto
* format: (opcional) Formato de saída. html ou json. Padrão: json