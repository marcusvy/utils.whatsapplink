<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitaf4538fbbe94680c85c2a87241df3e1c
{
    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'Marcusvy\\WhatsApp\\' => 18,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Marcusvy\\WhatsApp\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitaf4538fbbe94680c85c2a87241df3e1c::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitaf4538fbbe94680c85c2a87241df3e1c::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitaf4538fbbe94680c85c2a87241df3e1c::$classMap;

        }, null, ClassLoader::class);
    }
}
