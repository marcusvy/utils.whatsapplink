<?php

// autoload_real.php @generated by Composer

class ComposerAutoloaderInitaf4538fbbe94680c85c2a87241df3e1c
{
    private static $loader;

    public static function loadClassLoader($class)
    {
        if ('Composer\Autoload\ClassLoader' === $class) {
            require __DIR__ . '/ClassLoader.php';
        }
    }

    /**
     * @return \Composer\Autoload\ClassLoader
     */
    public static function getLoader()
    {
        if (null !== self::$loader) {
            return self::$loader;
        }

        spl_autoload_register(array('ComposerAutoloaderInitaf4538fbbe94680c85c2a87241df3e1c', 'loadClassLoader'), true, true);
        self::$loader = $loader = new \Composer\Autoload\ClassLoader(\dirname(__DIR__));
        spl_autoload_unregister(array('ComposerAutoloaderInitaf4538fbbe94680c85c2a87241df3e1c', 'loadClassLoader'));

        require __DIR__ . '/autoload_static.php';
        call_user_func(\Composer\Autoload\ComposerStaticInitaf4538fbbe94680c85c2a87241df3e1c::getInitializer($loader));

        $loader->register(true);

        return $loader;
    }
}
