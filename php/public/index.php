<?php
require_once "vendor/autoload.php";

use Marcusvy\WhatsApp\Generator;
use Marcusvy\WhatsApp\Http;


$generator = new Generator();
$http = new Http($generator);
$http->load();
$output = $http->display();


$format = $_GET['format'] ?? 'json';
switch($format) {
  case 'json':
    header("Content-Type: application/json;encoding=UTF-8");
    header("Access-Control-Allow-Origin: *");
    echo json_encode($output).PHP_EOL;
    break;

  case 'html':
    header("Content-Type: text/html;encoding=UTF-8");
    @ob_start();
    $url = $output['url'];
    require_once __DIR__."/../template/layout.php";
    ob_end_flush();
    break;

}
