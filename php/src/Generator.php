<?php

namespace Marcusvy\WhatsApp;

class Generator implements GeneratorInterface
{
  public function generate(string $phone, ?string $text = null, bool $as_web = false): string
  {
    $api_prefix = $this->get_api_prefix($as_web, $phone);
    $phone_url = $phone;
    if (is_null($text)) {
      return sprintf("%s%s", $api_prefix, $phone_url);
    }
    $encoded_text_url = urlencode($text);
    return sprintf("%s%s?text=%s", $api_prefix, $phone_url, $encoded_text_url);
  }

  private function get_api_prefix(bool $as_web = false, string $phone): string
  {
    return $as_web ? "https://web.whatsapp.com/send/?phone=$phone&type=phone_number&app_absent=0" : "https://wa.me/";
  }
}
