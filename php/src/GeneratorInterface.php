<?php

namespace Marcusvy\WhatsApp;

interface GeneratorInterface
{
  public function generate(string $phone, ?string $text=null): string;
}
