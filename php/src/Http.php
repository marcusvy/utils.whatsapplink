<?php

namespace Marcusvy\WhatsApp;

class Http
{
  private string $phone = '';
  private ?string $text = null;
  private GeneratorInterface $generator;

  public function __construct(GeneratorInterface $generator)
  {
    $this->generator = $generator;
  }

  public function load()
  {
    $this->phone = $_GET['phone'] ?? '';
    $this->text = $_GET['text'] ?? null;
  }

  public function display(): array
  {
    $output = [];
    if (empty($this->phone)) {
      $output['error'] = "Digite um telefone!";
    } else {
      // Generator
      $output['url'] = $this->generator->generate($this->phone, $this->text);
    }
    return $output;
  }
}
