<?php

namespace Marcusvy\WhatsApp;

class Console
{
  private string $phone = '';
  private ?string $text = null;
  private bool $as_web = false;
  private GeneratorInterface $generator;

  public function __construct(GeneratorInterface $generator)
  {
    $this->generator = $generator;
  }

  public function load()
  {
    $options = getopt("p:t:w:", ["phone:", "text:", "web:"]);
    $this->phone = array_key_exists("p", $options) ? $options['p'] : '';
    $this->text = array_key_exists("t", $options) ? $options['t'] : null;
    $this->as_web = array_key_exists("w", $options) ? $options['w'] : false;
  }

  public function display()
  {
    echo PHP_EOL;
    echo str_repeat("-", 40) . PHP_EOL;
    echo " Link do WhatsApp " . PHP_EOL;
    echo PHP_EOL;

    if (empty($this->phone)) {
      echo "Você precisa digitar um telefone válido! Exemplo: 5569984990000" . PHP_EOL;
    } else {
      // Generator
      $url = $this->generator->generate($this->phone, $this->text, $this->as_web);
      echo $url . PHP_EOL;
    }
    echo str_repeat("-", 40) . PHP_EOL;
    echo PHP_EOL;
  }
}
