<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>WhatsApp Link Generator</title>
  <style>
    /* Color Theme Swatches in Hex */
    .color-dark {
      color: #010221;
    }

    .color-primary {
      color: #0A7373;
    }

    .color-light {
      color: #B7BF99;
    }

    .color-warn {
      color: #EDAA25;
    }

    .color-error {
      color: #C43302;
    }

    .color-white {
      color: #ffffff;
    }

    .bg-dark {
      background-color: #010221;
    }

    .bg-primary {
      background-color: #0A7373;
    }

    .bg-light {
      background-color: #B7BF99;
    }

    .bg-warn {
      background-color: #EDAA25;
    }

    .bg-error {
      background-color: #C43302;
    }

    html,
    body {
      margin: 0;
      padding: 0;
      border: none;
      font: 16px/1.5em "Roboto";
      color: #333;
      min-height: 100vh;
      min-width: 100vw;
    }

    * {
      box-sizing: border-box;
    }

    .box {
      padding: 1rem;
      display: flex;
      gap: 0.5rem;
    }

    .box p {
      margin: 0;
      padding: 0;
    }

    .box__title {
      justify-content: center;
    }

    .box__icon {
      display: flex;
      width: 32px;
    }
    .box__icon svg {
      width: 100%;
      outline-color: inherit;
    }

    .box__content {
      display: flex;
      flex-direction: column;
    }

    .box__error {
      font-size: 1.2em;
      justify-content: center;
      align-items: center;
      flex-direction: column;
      text-align: center;
    }

    .box__success {
      border: 0;
      display: flex;
      justify-content: center;
    }

    .box__link {
      text-decoration: none;
      color: inherit;
      align-items: center;
      justify-content: center;
      display: flex;
    }

    section.content,
    footer {
      display: grid;
      width: 100%;
      max-width: 450px;
      margin: 0 auto;
      padding-left: 1rem;
      padding-right: 1rem;
      align-items: center;
      justify-content: center;
    }

    footer {
      padding: 2rem 1rem;
    }

    form.form {
      padding-top: 1rem;
    }

    .form-field {
      display: flex;
      flex-direction: column;
      margin-bottom: 0.5rem;
    }

    .form-control {
      display: flex;
      flex-grow: 1;
      border-radius: 5px;
      border: 1px solid #666;
      padding: 0.5rem;
      background-color: #f5f5f5;
      border: 1px solid #eee;
      margin-top: 0.5rem;
      margin-bottom: 0.5rem;
    }

    .form-field input {
      min-height: 32px;
      border: 0;
    }

    .form-field textarea {
      min-height: 64px;
    }

    .form-field label {
      display: flex;
      flex-grow: 1;
      align-items: center;
      min-height: 32px;
    }

    .form-field input[type="text"],
    .form-field input[type="tel"],
    .form-field textarea {
      height: 32px;
      border: 1px solid #ddd;
    }

    .form-field input[type="submit"],
    .form-field input[type="reset"] {
      height: 32px;
      flex-grow: 0;
      border: 0;
      border-radius: 5px;
    }
  </style>
</head>

<body>

  <div class="box box__title bg-primary color-white">
  <h1>WhatsApp Link Generator</h1>
  </div>

  <?php if (isset($output['error']) && $output['error'] !== null) : ?>

  <div class="box box__error bg-warn">
    <p><?= $output['error'] ?></p>
  </div>

  <?php else : ?>

  <a class="box box__link box__success bg-light" target="_blank" href="<?= $url ?>">
    <div class="box__icon">
      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <path stroke-linecap="round" stroke-linejoin="round" d="M20.25 8.511c.884.284 1.5 1.128 1.5 2.097v4.286c0 1.136-.847 2.1-1.98 2.193-.34.027-.68.052-1.02.072v3.091l-3-3c-1.354 0-2.694-.055-4.02-.163a2.115 2.115 0 01-.825-.242m9.345-8.334a2.126 2.126 0 00-.476-.095 48.64 48.64 0 00-8.048 0c-1.131.094-1.976 1.057-1.976 2.192v4.286c0 .837.46 1.58 1.155 1.951m9.345-8.334V6.637c0-1.621-1.152-3.026-2.76-3.235A48.455 48.455 0 0011.25 3c-2.115 0-4.198.137-6.24.402-1.608.209-2.76 1.614-2.76 3.235v6.226c0 1.621 1.152 3.026 2.76 3.235.577.075 1.157.14 1.74.194V21l4.155-4.155" />
      </svg>
    </div>
    <div class="box__content">
      <p>Clique aqui para acessar</p>
    </div>
  </a>
  <?php endif; ?>

  <section class="content">
    <form class="form" action="index.php">

      <input type="hidden" name="format" value="html">

      <div class="form-field">
        <label for="input-phone">Telefone</label>
        <input class="form-control" id="input-phone" autofocus type="tel" name="phone" placeholder="5569984990000"
          <?php if (isset($_GET['phone']) && $_GET['phone']) {
            echo sprintf('value="%s"', htmlspecialchars($_GET['phone']));
          } ?>/>
      </div>
      <div class="form-field">
        <label for="input-text">Texto</label>
        <textarea class="form-control" id="input-text" name="text"><?php
        if (isset($_GET['text']) && $_GET['text']) {
          echo htmlspecialchars($_GET['text']);
        }
        ?></textarea>
      </div>

      <div class="form-field">
        <input type="submit" value="Gerar" class="bg-primary color-white">
      </div>

      <a class="box__link" href="/?format=html" style="margin-top: 1rem">
        Tentar novamente
      </a>
    </form>
  </section>
  <footer>
    <p class="color-dark">
      @Copyright 2022 <br>
      Marcus Vinícius da R G Cardoso <br>
      <a href="mailto:marcusvy@gmail.com">marcusvy@gmail.com</a>
    </p>
  </footer>
</body>

</html>